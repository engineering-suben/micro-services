#!/bin/bash

mv /etc/filebeat/filebeat.yml /etc/filebeat/filebeat-backup.yml
cp ./filebeat-dev.yml /etc/filebeat/filebeat.yml
chown root /etc/filebeat/filebeat.yml
chmod 644 /etc/filebeat/filebeat.yml

mkdir -p /etc/pki/tls/certs
cp ./logstash-beats.crt /etc/pki/tls/certs/logstash-beats.crt

service filebeat start

export PATH=$PATH:/opt/gradle/gradle-7.1/bin

#gradle build --continuous &
gradle buildAndReload --continuous &

gradle bootRun -D spring.profiles.active=dev
