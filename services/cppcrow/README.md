building via CLI
To build a crow Project, do the following:

GCC (G++)
Release: g++ ./src/main.cpp -lpthread -lboost_system
Debug: g++ ./src/main.cpp -ggdb -lpthread -lboost_system -DCROW_ENABLE_DEBUG
SSL: g++ ./src/main.cpp -lssl -lcrypto -lpthread -lboost_system -DCROW_ENABLE_SSL
Clang
Release: clang++ ./src/main.cpp -lpthread -lboost_system
Debug: clang++ ./src/main.cpp -g -lpthread -lboost_system -DCROW_ENABLE_DEBUG
SSL: clang++ ./src/main.cpp -lssl -lcrypto -lpthread -lboost_system -DCROW_ENABLE_SSL
