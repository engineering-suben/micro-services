import mongoose from "mongoose";

const userSchema = mongoose.Schema({
	email: {
		type: String,
		lowercase: true,
		required: true,
	},
	password: { type: String, required: true },
});

class User {
	constructor() {
		this.User = mongoose.model("User", userSchema, "Users");
	}

	create(user) {
		const newUser = new this.User(user);
		return newUser.save();
	}

	findOneByEmail(email) {
		return this.User.findOne(email);
	}
}

export default new User();
