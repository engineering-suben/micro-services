import dotenv from "dotenv";
import express from "express";
import boom from "express-boom";
import cors from "cors";
import initMongoDB from "./services/mongodb";
import { initMySql } from "./services/mysql";
import { initPostgres } from "./services/postgres";
import apiRouter from "./routes";
import { authMiddleware } from "./middlewares/authMiddleware";

dotenv.config();

const PORT = process.env.PORT || 80;

const app = express();

app.use(boom());
app.use(cors());
app.disable("etag");

app.use("/api", apiRouter);

app.listen(PORT, () => {
	console.log("Listening on port: " + PORT);
});

initMongoDB();
initMySql();
initPostgres();