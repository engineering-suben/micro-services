import userModel from "../models/user";
import { hashPassword, checkPassword } from "../utils/password";
import { generateToken } from "../utils/token";
import CustomError from "../utils/CustomError";

export const signupController = async (user) => {
	const password = await hashPassword(user.password);

	const doesUsrExist = await userModel.findOneByEmail({ email: user.email });

	if (doesUsrExist) {
		throw new CustomError("badRequest", "User Already Exists.");
	}

	const usr = await userModel.create({
		email: user.email,
		password: password,
	});

	if (usr) {
		const token = generateToken({ email: user.email });

		return { email: user.email, token };
	} else {
		throw new CustomError("badImplementation", "Failed to create user.");
	}
};

export const loginController = async (user) => {
	const usr = await userModel.findOneByEmail({ email: user.email });

	if (usr) {
		const checkRes = await checkPassword(user.password, usr.password);
		if (!checkRes) {
			throw new CustomError("badRequest", "Invalid Credentials.");
		}

		const token = generateToken({ email: user.email });

		return { email: user.email, token };
	} else {
		throw new CustomError("badRequest", "Invalid Credentials.");
	}
};
