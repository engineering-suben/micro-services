import Sequelize from "sequelize";

let sequelize;

export const initMySql = () => {
	sequelize = new Sequelize({
		host: "mysql",
		port: 3306,
		username: "mysqluser",
		password: "mysqluser",
		database: "orders",
		dialect: "mysql"
	});
	sequelize.authenticate()
	.then(() => {
		console.log("Connected to MySql!");
	})
	.catch(() => {
		console.log("Connection to MySql failed!");
	});
}

export default sequelize;