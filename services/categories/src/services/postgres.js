import Sequelize from "sequelize";

let sequelize;

export const initPostgres = () => {
	sequelize = new Sequelize({
		host: "postgres",
		port: 5432,
		username: "postgresuser",
		password: "postgresuser",
		database: "orderItems",
		dialect: "postgres"
	});
	sequelize.authenticate()
	.then(() => {
		console.log("Connected to Postgres!");
	})
	.catch(() => {
		console.log("Connection to Postgres failed!");
	});
}

export default sequelize;